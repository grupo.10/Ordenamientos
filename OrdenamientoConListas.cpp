#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
#include <cstdlib>
#include <string>
#include <cctype>

using namespace std;

struct nodo {
	int num;
	struct nodo *sgte;
	struct nodo *ant;
};
typedef struct nodo *Tlista;
typedef struct nodo *cola;
void gotoxy(int, int);
void menu();
void insertarInicio(Tlista &, int);
void insertarFinal(Tlista &, int);
void insertarEnPosicion(Tlista &, int);
void insertarAntesoDespues(Tlista &, int, int);
void ordenamientoInsercion(Tlista &, int);
void ordenamientoShell(Tlista &);
void eliminarInicio(Tlista &);
void eliminarFinal(Tlista &);
void eliminarPorPosicion(Tlista &, int);
void mostrar(Tlista);
int tamano(Tlista);
bool esNumerico(string);
int devuelveValor(Tlista &, int);
void insertaValor(Tlista &, int, int);
int cantVueltas(int);
void ordenamientoSeleccion(Tlista &);
void ordenamientoBurbuja(Tlista &);
void ordenamientoIntercambio(Tlista &);
void ordenamientoRadix(Tlista &);
int digito(int, int);
int extraer(cola &);
void adicionar(cola &, int);
int extraer(cola &);
cola crear_nodo_c(int);
Tlista mergeSort(Tlista &);
void ordenamientoMergeSort(Tlista &);
void EliminarLista(Tlista &);
Tlista intercalar(Tlista, Tlista);
int particion(Tlista &, int, int);
void quickSort(Tlista &, int, int);
void ordenamientoQuickSort(Tlista &);
Tlista seleccion(Tlista, int);

bool esNumerico(string linea)
{
	bool b = true;
	int longitud = linea.size();

	if (longitud == 0) {
		b = false;
	}
	else if (longitud == 1 && !isdigit(linea[0])) {
		b = false;
	}
	else {
		int i;
		if (linea[0] == '+' || linea[0] == '-')
			i = 1;
		else
			i = 0;

		while (i < longitud) {
			if (!isdigit(linea[i])) {
				b = false;
				break;
			}
			i++;
		}
	}
	return b;
}

int main() {
	Tlista lista = NULL;
	string valor, pos, resp;
	int r;
	do {
		mostrar(lista);
		menu(); cin >> resp;
		if (esNumerico(resp))
		{
			r = atoi(resp.c_str());
			switch (r) {
			case 0:
				//pruebas
				insertarFinal(lista, 3);
				insertarFinal(lista, 8);
				insertarFinal(lista, 5);
				insertarFinal(lista, 7);
				insertarFinal(lista, 2);
				insertarFinal(lista, 9);
				insertarFinal(lista, 1);
				insertarFinal(lista, 4);
				insertarFinal(lista, 0);
				insertarFinal(lista, 10);
				cout << "great" << endl;
				break;
			case 1:
				cout << "Ingresar valor a ingresar :    "; cin >> valor;
				if (esNumerico(valor))
				{
					int b = atoi(valor.c_str());
					insertarInicio(lista, b);
					cin.get();
					cout << "Ingreso correcto" << endl;
					mostrar(lista);
				}
				else
					cout << "Valor ingresado no numerico" << endl;
				break;
			case 2:
				cout << "Ingresar valor a ingresar :    "; cin >> valor;
				if (esNumerico(valor))
				{
					int b = atoi(valor.c_str());
					insertarFinal(lista, b);
					cout << "Ingreso correcto" << endl;
					mostrar(lista);
				}
				else
					cout << "Valor ingresado no numerico" << endl;
				break;
			case 3:
				cout << "Ingresar la posicion : "; cin >> pos;
				if (esNumerico(pos))
				{
					int b = atoi(pos.c_str());
					insertarEnPosicion(lista, b);
					mostrar(lista);
				}
				else
					cout << "Valor ingresado no numerico" << endl;
				break;
			case 4:
				cout << "Ingresar valor a ingresar : "; cin >> valor;
				if (esNumerico(valor))
				{
					int b = atoi(valor.c_str());
					ordenamientoInsercion(lista, b);
					cout << "Valor ingresado y ordenado" << endl;
					mostrar(lista);
				}
				else
					cout << "Valor ingresado no numerico" << endl;
				break;
			case 5:
				ordenamientoSeleccion(lista);
				cout << "Lista ordenada" << endl;
				mostrar(lista);
				break;
			case 6:
				ordenamientoBurbuja(lista);
				cout << "Lista ordenada" << endl;
				mostrar(lista);
				break;
			case 7:
				ordenamientoIntercambio(lista);
				cout << "Lista ordenada" << endl;
				mostrar(lista);
				break;
			case 8:
				ordenamientoShell(lista);
				cout << "Lista ordenada" << endl;
				mostrar(lista);
				break;
			case 9:
				ordenamientoRadix(lista);
				cout << "Lista ordenada" << endl;
				mostrar(lista);
				break;
			case 10:
				ordenamientoMergeSort(lista);
				cout << "Lista ordenada" << endl;
				mostrar(lista);
				break;
			case 11:
				ordenamientoQuickSort(lista);
				cout << "Lista ordenada" << endl;
				mostrar(lista);
				break;
			case 12:
				eliminarInicio(lista);
				cout << "Eliminado el primer valor" << endl;
				mostrar(lista);
				break;
			case 13:
				eliminarFinal(lista);
				cout << "Eliminado el ultimo valor" << endl;
				mostrar(lista);
				break;
			case 14:
				cout << "Ingresar la posicion : "; cin >> pos;
				if (esNumerico(valor))
				{
					int b = atoi(pos.c_str());
					eliminarPorPosicion(lista, b);
					mostrar(lista);
				}
				else
					cout << "Valor ingresado no numerico" << endl;
				break;
			case 15:
				EliminarLista(lista);
				cout << "Lista Eliminada" << endl;
				mostrar(lista);
				break;
			}

		}
		else
			cout << "Valor ingresado no numerico" << endl;
		system("pause>nul");
		system("cls");
	} while (r != 16);
	return 0;
}

void menu() {
	gotoxy(0, 0); cout << "\t  Listas Enlazadas Simples" << endl;
	gotoxy(0, 1); cout << "1. Ingresar valor al inicio" << endl;
	gotoxy(0, 2); cout << "2. Ingresar valor al final" << endl;
	gotoxy(0, 3); cout << "3. Ingresar entre valor" << endl;
	gotoxy(0, 4); cout << "4. Ordenar Por Inserccion" << endl;
	gotoxy(0, 5); cout << "5. Ordenar Por Seleccion" << endl;
	gotoxy(0, 6); cout << "6. Ordenar Por Burbuja" << endl;
	gotoxy(0, 7); cout << "7. Ordenar Por Intercambio" << endl;
	gotoxy(0, 8); cout << "8. Ordenar Shellsort" << endl;
	gotoxy(0, 9); cout << "9. Ordenar Radix" << endl;
	gotoxy(0, 10); cout << "10. Ordenar Mergesort" << endl;
	gotoxy(0, 11); cout << "11. Ordenar Quicksort" << endl;
	gotoxy(0, 12); cout << "12. Eliminar el inicio" << endl;
	gotoxy(0, 13); cout << "13. Eliminar el final" << endl;
	gotoxy(0, 14); cout << "14. Eliminar por posicion" << endl;
	gotoxy(0, 15); cout << "15. LIMPIAR LISTA" << endl;
	gotoxy(0, 16); cout << "16. Salir" << endl;
	gotoxy(0, 17); cout << "Ingresar opcion : ";
}

void gotoxy(int x, int y) {
	HANDLE hcon;
	hcon = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD dwPos;
	dwPos.X = x;
	dwPos.Y = y;
	SetConsoleCursorPosition(hcon, dwPos);
}

void insertarInicio(Tlista &lista, int valor) {
	Tlista nuevo = new(struct nodo);
	nuevo->num = valor;
	nuevo->sgte = NULL;
	nuevo->ant = NULL;

	if (lista != NULL) {
		nuevo->sgte = lista;
		lista->ant = nuevo;
		lista = nuevo;
	}
	else
		lista = nuevo;
}

void mostrar(Tlista lista) {
	int i = 60, j = 1;
	if (lista != NULL)
	{

		while (lista != NULL) {
			gotoxy(i, j); cout << lista->num << endl;
			lista = lista->sgte;
			j++;
		}
		gotoxy(i, j);
	}
}

void insertarFinal(Tlista &lista, int valor) {
	Tlista temp = lista;
	Tlista nuevo = new(struct nodo);
	nuevo->num = valor;
	nuevo->sgte = NULL;
	nuevo->ant = NULL;
	if (lista != NULL) {
		while (temp->sgte != NULL) {
			temp = temp->sgte;
		}
		temp->sgte = nuevo;
		nuevo->ant = temp;
	}
	else {
		lista = nuevo;
	}
}
void insertarEnPosicion(Tlista &lista, int pos) {
	Tlista temp = lista;
	int cont = 0;
	string valor;
	if (lista == NULL) {
		cout << "Lista Vacia" << endl;
	}
	else {
		while (temp != NULL && cont<pos - 1) {
			temp = temp->sgte;
			cont++;
		}
		if (temp == NULL)
			cout << "Posicion no valida" << endl;
		else {
			cout << "Posicion encontrada" << endl;
			cout << "Ingresar valor a ingresar : "; cin >> valor;
			if (esNumerico(valor))
			{
				int b = atoi(valor.c_str());
				insertarAntesoDespues(lista, b, pos);
				cout << "Ingreso correcto" << endl;
			}
			else
				cout << "Valor ingresado no numerico" << endl;

		}
	}
}

void insertarAntesoDespues(Tlista &lista, int valor, int pos) {
	int rpta, cont = 0;
	Tlista nuevo = new(struct nodo), temp = lista, k = NULL;
	nuevo->num = valor;
	nuevo->sgte = NULL;

	pos--;
	while (cont<pos) {
		k = temp;
		temp = temp->sgte;
		cont++;
	}

	k->sgte = nuevo;
	nuevo->ant = k;
	nuevo->sgte = temp;
	temp->ant = nuevo;

}

void ordenamientoInsercion(Tlista &lista, int valor) {
	Tlista temp = lista, nuevo = new(struct nodo), k = NULL;
	bool bu = true;
	nuevo->num = valor;
	nuevo->sgte = NULL;
	nuevo->ant = NULL;
	if (lista != NULL) {
		while (temp != NULL && bu == true) {
			if (valor <= temp->num) {
				bu = false;
			}
			else {
				k = temp;
				temp = temp->sgte;
			}

		}
		if (temp == lista) {
			insertarInicio(lista, valor);
		}
		else {
			k->sgte = nuevo;
			nuevo->ant = k;
			nuevo->sgte = temp;
			temp->ant = nuevo;
		}

	}
	else
		lista = nuevo;
}

void eliminarInicio(Tlista &lista) {
	Tlista temp = lista;
	lista = lista->sgte;
	delete(temp);
}

void eliminarFinal(Tlista &lista) {
	Tlista temp = lista, k = NULL;
	while (temp->sgte != NULL) {
		k = temp;
		temp = temp->sgte;
	}
	k->sgte = NULL;
	delete(temp);
}

void eliminarPorPosicion(Tlista &lista, int pos) {
	Tlista temp = lista, k = NULL;
	int cont = 0;
	while (temp != NULL && cont<pos - 1) {
		k = temp;
		temp = temp->sgte;
		cont++;
	}
	if (temp == NULL)
		cout << "Posicion no valida" << endl;
	else {
		if (pos == 1)
			eliminarInicio(lista);
		else {
			k->sgte = temp->sgte;
			delete(temp);
			cout << "Eliminado el elemento de la posicion " << pos << endl;
		}
	}
}

int tamano(Tlista lista) {
	int cont = 0;
	while (lista != NULL) {
		lista = lista->sgte;
		cont++;
	}
	return cont;
}

void ordenamientoShell(Tlista &cabecera) {
	Tlista nodoAux = cabecera;
	int size = tamano(cabecera);

	int aux = size;
	for (int i = 0; i <= cantVueltas(size); i++) {
		if (aux != 1) {
			aux = aux / 2;
		}

		for (int j = 1; j <= size - aux; j++) {

			int n1 = devuelveValor(nodoAux, j);
			int n2 = devuelveValor(nodoAux, j + aux);
			if (n1>n2) {
				int valAux = n1;
				insertaValor(nodoAux, j, n2);
				insertaValor(nodoAux, j + aux, valAux);
			}
		}
	}
}

int devuelveValor(Tlista &cabecera, int indice) {
	int value;
	Tlista nodoAux = cabecera;
	for (int i = 0; i<indice; i++) {

		value = nodoAux->num;
		nodoAux = nodoAux->sgte;
	}
	return value;


}
void insertaValor(Tlista &cabecera, int indice, int valor) {
	Tlista nodoaux = cabecera;
	for (int i = 0; i<indice - 1; i++) {
		nodoaux = nodoaux->sgte;
	}
	nodoaux->num = valor;
}
int cantVueltas(int size) {
	int i = 0;
	while (size != 0) {

		i++;
		size = size / 2;
	}
	return i - 1;
}

void ordenamientoSeleccion(Tlista &lista) {
	int tempi;
	Tlista aux = lista;
	if (lista == NULL) {
		cout << "Lista Vacia" << endl;
	}
	else {
		while (aux->sgte != NULL) {
			Tlista tmp = aux;
			while (tmp->sgte != NULL) {
				if (aux->num>tmp->sgte->num) {
					tempi = aux->num;
					aux->num = tmp->sgte->num;
					tmp->sgte->num = tempi;
				}
				tmp = tmp->sgte;
			}
			aux = aux->sgte;
		}
	}
}

void ordenamientoBurbuja(Tlista &lista) {
	Tlista actual, siguiente;
	int t;
	actual = lista;
	while (actual->sgte != NULL)
	{
		siguiente = actual->sgte;
		while (siguiente != NULL)
		{
			if (actual->num > siguiente->num)
			{
				t = siguiente->num;
				siguiente->num = actual->num;
				actual->num = t;
			}
			siguiente = siguiente->sgte;
		}
		actual = actual->sgte;
		siguiente = actual->sgte;
	}
}

void ordenamientoIntercambio(Tlista &lista) {
	Tlista inicio = lista, temp = NULL, aux = NULL;
	int auxi;
	while (inicio->sgte != NULL) {
		temp = inicio;
		aux = temp;
		while (temp->sgte != NULL) {
			if (aux->num > temp->sgte->num) {
				auxi = aux->num;
				aux->num = temp->sgte->num;
				temp->sgte->num = auxi;
			}
			temp = temp->sgte;
		}
		inicio = inicio->sgte;
	}
}

void EliminarLista(Tlista &lista) {
	Tlista aux = NULL;
	while (lista != NULL) {
		aux = lista;
		lista = lista->sgte;
		delete(aux);
	}
}


void ordenamientoRadix(Tlista &lista) {
	Tlista temp = lista, final;
	cola c[10] = { NULL };
	int valor, i = 1;
	boolean estado;
	do {
		estado = false;
		final = NULL;
		while (temp != NULL) {
			valor = digito(temp->num, i);
			adicionar(c[valor], temp->num);
			if (valor != 0)
				estado = true;
			temp = temp->sgte;
		}
		for (int j = 0; j < 10; j++) {
			while (c[j] != NULL) {
				insertarFinal(final, extraer(c[j]));
			}
		}
		temp = final;
		i++;
	} while (estado);
	lista = temp;
}

int digito(int valor, int i) {
	int j = 0, dig = 0;
	while (j < i) {
		dig = valor % 10;
		valor = valor / 10;
		j++;
	}
	return dig;
}

int extraer(cola &c) {
	int aux = c->num;
	c = c->sgte;
	return aux;
}

void adicionar(cola &c, int valor) {
	cola aux = crear_nodo_c(valor);
	if (c == NULL) {
		c = aux;
	}
	else {
		cola temp = c;
		while (temp->sgte != NULL)
			temp = temp->sgte;
		temp->sgte = aux;
	}
}


cola crear_nodo_c(int valor) {
	cola aux = new(struct nodo);
	aux->sgte = NULL;
	aux->num = valor;
	return aux;
}

Tlista mergeSort(Tlista &lista) {
	if (tamano(lista) <= 1) {
		return lista;
	}
	else {
		Tlista izq = lista, der = lista;
		int medio = tamano(lista) / 2;
		for (int i = 1; i <= medio; i++) {
			der = der->sgte;
		}
		der->ant->sgte = NULL;
		der->ant = NULL;
		izq = mergeSort(izq);
		der = mergeSort(der);
		return intercalar(izq, der);
	}
}
void ordenamientoMergeSort(Tlista &lista) {
	lista = mergeSort(lista);
}

Tlista intercalar(Tlista ordizq, Tlista ordder) {
	Tlista result = NULL;
	while (ordizq != NULL && ordder != NULL) {
		if (ordizq->num<ordder->num) {
			insertarFinal(result, ordizq->num);
			ordizq = ordizq->sgte;
		}
		else {
			insertarFinal(result, ordder->num);
			ordder = ordder->sgte;
		}
	}
	while (ordizq != NULL) {
		insertarFinal(result, ordizq->num);
		ordizq = ordizq->sgte;
	}
	while (ordder != NULL) {
		insertarFinal(result, ordder->num);
		ordder = ordder->sgte;
	}
	EliminarLista(ordizq);
	EliminarLista(ordder);
	return result;
}

int particion(Tlista &l1, int izq, int der) {
	int i = izq, j = der;
	int tmp;
	int pivot = seleccion(l1, (izq + der) / 2)->num;
	while (i <= j) {
		while (seleccion(l1, i)->num < pivot)
			i++;
		while (seleccion(l1, j)->num > pivot)
			j--;
		if (i <= j) {
			tmp = seleccion(l1, i)->num;
			seleccion(l1, i)->num = seleccion(l1, j)->num;
			seleccion(l1, j)->num = tmp;
			i++;
			j--;
		}
	}
	return i;
}
void quickSort(Tlista &lista, int izq, int der) {
	int indice = particion(lista, izq, der);
	if (izq< indice - 1)
		quickSort(lista, izq, indice - 1);
	if (indice < der)
		quickSort(lista, indice, der);
}
void ordenamientoQuickSort(Tlista &lista) {
	quickSort(lista, 0, tamano(lista) - 1);
}

Tlista seleccion(Tlista lista, int i) {
	for (int j = 0; j<i; j++)
		lista = lista->sgte;
	return lista;
}